# Generated by Django 4.0.3 on 2022-04-21 17:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0002_accountvo'),
    ]

    operations = [
        migrations.AddField(
            model_name='attendee',
            name='account',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='attendees', to='attendees.accountvo'),
        ),
    ]
