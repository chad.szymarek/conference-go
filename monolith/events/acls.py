import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture_url(location):
    params = {"query": location}
    response = requests.get(
        "https://api.pexels.com/v1/search",
        headers={"Authorization": PEXELS_API_KEY},
        params=params,
    )

    pictures = json.loads(response.content)

    photo_url = {"url": pictures["photos"][0]["url"]}

    return photo_url


def get_weather_data(city, state):
    response_geo = requests.get(
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + "&appid="
        + OPEN_WEATHER_API_KEY
    )

    data_geo = json.loads(response_geo.content)[0]
    lon = data_geo["lon"]
    lat = data_geo["lat"]

    response_weather = requests.get(
        "https://api.openweathermap.org/data/2.5/weather?lat="
        + str(lat)
        + "&lon="
        + str(lon)
        + "&appid="
        + OPEN_WEATHER_API_KEY
    )

    data_weather = json.loads(response_weather.content)

    print(data_weather)

    return data_weather
