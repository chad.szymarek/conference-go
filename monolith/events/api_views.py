import re
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


from common.json import ModelEncoder
from .models import Conference, Location, State
from .acls import get_picture_url, get_weather_data


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "state",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        # get the location object and put it in the context dictionary
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)

        conference = Conference.objects.create(**content)
        return JsonResponse(
            {"conference": conference},
            encoder=ConferenceDetailEncoder,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_conference(request, pk):
    """
    Returns the details for the Conference model specified
    by the pk parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    if request.method == "GET":
        conference = Conference.objects.get(id=pk)

        city = conference.location.city
        state = conference.location.state.name
        weather = get_weather_data(city, state)
        return JsonResponse(
            {
                "conference": conference,
                "weather": {
                    "temp": weather["main"]["temp"],
                    "description": weather["weather"][0]["description"],
                },
            },
            encoder=ConferenceDetailEncoder,
        )

    elif request.method == "DELETE":

        count, _ = Conference.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})

    else:
        content = json.loads(request.body)

        Conference.objects.filter(id=pk).update(**content)

        conference = Conference.objects.get(id=pk)
        return JsonResponse(
            {"conference": conference}, encoder=ConferenceDetailEncoder
        )


# allows only GET and POST requirests to this method
@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        # Code below will only run if the request is a GET request
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations}, encoder=LocationListEncoder
        )
    else:
        # Converts the JSON-formatted string in request.body into a python dictionary
        content = json.loads(request.body)
        try:
            # get the State object and put it in the content dictionary
            # based on the state string that is already in the dictionary from the request
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state

            # if the state does not exisit, send this error json response
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )

        # sets the city variable to the city in the content of the json
        city = content["city"]

        # runs the get_picture_url function, passing the parameter of
        # the city variable to it
        picture = get_picture_url(city)

        # sets the picture url key = to the picture that is returned in
        # line above
        content["picture_url"] = picture

        # creates a new location using the dictionary we just made with json.loads
        location = Location.objects.create(**content)

        return JsonResponse(
            {"location": location}, encoder=LocationDetailEncoder
        )


# allows only a DELETE, GET, PUT http requests
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, pk):
    """
    Returns the details for the Location model specified
    by the pk parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    # code below will only run if the request is a GET
    if request.method == "GET":
        location = Location.objects.get(id=pk)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )

    # code below will only run if the request is a DELETE
    elif request.method == "DELETE":
        # count stores the number of objects delete
        # _ stores a dictionary with the number of deletions per object type
        count, _ = Location.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # this code handles the PUT request
        # convert the submitted JSON-formatted string into a python dictionary
        content = json.loads(request.body)

        try:
            # convert the state abbreviation into a State if it exists
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )

        # use that content dictionary to update the existing location
        Location.objects.filter(id=pk).update(**content)

        location = Location.objects.get(id=pk)
        return JsonResponse(
            location, encoder=LocationDetailEncoder, safe=False
        )
